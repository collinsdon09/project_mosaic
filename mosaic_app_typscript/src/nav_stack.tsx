import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Button,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import MapsScreen from "../components/map";
// import Circle from "./circle_safety";

import { PieChartWithCenteredLabels_2 } from "../components/pie2";

import Circle from "../components/circle_safety";
import { useRoute } from "@react-navigation/native";
import axios from "axios";
// import { PieChartWithCenteredLabels } from "./pie2";
import LineChartGraph from "../components/LineChartComponent";
import { Line } from "react-native-svg";

import { LineChart } from "react-native-chart-kit";

import { YAxisExample } from "../components/graphComponent";
//SCREEN A
// const Squares = ({navigation}) => {

// let face_array = [];

const MyComponent = () => {
  let face_array = [];

  const route = useRoute();
  const { loc_name } = route.params;

  //******************************************** */
  const [wearing, setWearing] = useState("");
  const [notWearing, setNotWearing] = useState("");
  const [generalSafetyIdx, setgeneralSafetyIdx] = useState(0.0);

  const [facialSafetyIdx, setfacialSafetyIdx] = useState(0.0);
  const [usersdata, setUsers] = useState([]);
  const [year, setYear] = useState(0);
  const [month, setMonth] = useState(0);
  const [day, setDay] = useState(0);
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(0);
  const [milliseconds, setMilliseconds] = useState(0);

  useEffect(() => {
    axios
      .get(`http://165.227.203.5/data-test/${loc_name}`)
      .then((response) => {
        // setRequestCount(requestCount + 1);

        const dataArray: any = Object.values(response.data);

        dataArray.forEach((obj) => {
          const date = new Date(obj.scan.time);
          // console.log("date", date.getFullYear());

          setgeneralSafetyIdx(parseFloat(obj.policy.safety_index));
          setWearing(obj.policy.wearing_mask);
          setNotWearing(obj.policy.no_mask);
          setYear(date.getFullYear());
          setMonth(date.getMonth());
          setDay(date.getDay());
          setHours(date.getHours());
          setMinutes(date.getMinutes());
          setSeconds(date.getSeconds());
          setMilliseconds(date.getMilliseconds());

          //   console.log(obj.scan.Faces)
          obj.scan.Faces.forEach((faceObj) => {
            // console.log(faceObj.face_safety_indx);
            setfacialSafetyIdx(faceObj.face_safety_indx);
            face_array.push(faceObj.face_safety_indx);

            setUsers(face_array);
          });
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const maskProps = {
    text: "Mask Usage",
    wearing: wearing,
    not_wearing: notWearing,
  };

  // rconsole.log(usersdata);

  const intArray = usersdata.map((str) => parseInt(str));

  return (
    <View>
      <Text style={styles.location_text}>{loc_name}</Text>
      <Circle safetyIndexProp={generalSafetyIdx} />
      <View style={{ flexDirection: "row" }}>
        <Text style={{ fontSize: 15, fontWeight: "bold", marginLeft: 10 }}>
          Year: {year}
        </Text>
        <Text style={{ fontSize: 15, fontWeight: "bold", marginLeft: 10 }}>
          Month: {month}
        </Text>
        <Text style={{ fontSize: 15, fontWeight: "bold", marginLeft: 10 }}>
          Day: {day}
        </Text>
        <Text style={{ fontSize: 15, fontWeight: "bold", marginLeft: 10 }}>
          Hour:{hours}
        </Text>
        <Text style={{ fontSize: 15, fontWeight: "bold", marginLeft: 10 }}>
          Min:{minutes}
        </Text>
        <Text style={{ fontSize: 15, fontWeight: "bold", marginLeft: 10 }}>
          Sec:{seconds}
        </Text>
        <Text style={{ fontSize: 15, fontWeight: "bold", marginLeft: 10 }}>
          millisec:{seconds}
        </Text>
      </View>
      <YAxisExample face_arrayProp={intArray} />

      <PieChartWithCenteredLabels_2 {...maskProps} />
      {/* <Text>{usersdata}</Text> */}
    </View>
  );
};

const Stack = createNativeStackNavigator();

function MainStack() {
  //main function
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={MapsScreen} />
        <Stack.Screen name="Details" component={MyComponent} />
        {/* <Stack.Screen name="HistoryPage" component={HistoryList} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 0,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor:'black',
    height: 200,
    // marginBottom:200,
  },
  square: {
    width: 35,
    height: 35,
    margin: 3,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  text: {
    fontSize: 10,
    color: "white",
  },

  circle: {
    // width: 100,
    // height: 100,
    // borderRadius: 50,
    backgroundColor: "blue",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: 20,
    right: 20,
    width: 50,
    height: 50,
    borderRadius: 25,
  },

  container2: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f2f2f2",
  },

  location_text: {
    fontSize: 40,
    marginLeft: 30,
    marginTop: 30,
  },
});

export default MainStack;
