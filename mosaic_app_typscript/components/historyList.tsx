import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { useRoute } from '@react-navigation/native';


const HistoryList = ({ navigation }) => {
    const route = useRoute();

    const { data_name } = route.params;

  return (
    <View style={styles.container}>
      {data_name.map((item, index) => (
        <View key={index} style={styles.listItem}>
          <Text style={styles.time}>{item.time}</Text>
          <Text style={styles.safetyIdx}>{item.safetyIdx}</Text>
        </View>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 20,
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
  },
  time: {
    fontSize: 18,
    fontWeight: 'bold',
    marginRight: 10,
  },
  safetyIdx: {
    fontSize: 18,
  },
});

export default HistoryList;
