

import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';


type safetyIndexProps = {
    safetyIndexProp: number;
  };


  export default function Circle(props: safetyIndexProps){

    let circleColor = 'red';
        if (props.safetyIndexProp > 40 && props.safetyIndexProp <= 59) {
            circleColor = 'orange';
        } else if (props.safetyIndexProp > 60 && props.safetyIndexProp <= 79) {
            circleColor = 'yellow';
        } else if (props.safetyIndexProp > 80 && props.safetyIndexProp <= 100) {
            circleColor = 'green';
        }
    return(

      <View style={[styles.circle, { backgroundColor: circleColor }]}>
        <Text> Safety index</Text>
        <Text style={styles.text}>{props.safetyIndexProp}</Text>
      </View>
    )
  
    


  }

const styles = StyleSheet.create({
  circle: {
    marginTop:10,
    marginBottom:50,
    marginLeft:300,
    width: 100,
    height: 100,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 32,
    fontWeight: 'bold',
  },
});
