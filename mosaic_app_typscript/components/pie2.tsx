import React from 'react'
import { PieChart } from 'react-native-svg-charts'
// import { Text } from 'react-native-svg'
import { Text as SvgText } from 'react-native-svg';

import { View, Text, StyleSheet, TouchableOpacity,Button } from 'react-native';

// type wearingMaskProp = {
//     wearing_mask_prop: number;
//   };

  interface MaskProps {
    text: string;
    wearing: number;
    not_wearing:number;
    // onPress: () => void;
  }



export class PieChartWithCenteredLabels_2 extends React.PureComponent<MaskProps> {

    

    render() {

    // console.log("from pie chart2:",this.props.wearing)
    // console.log("not wearing", this.props.not_wearing)


        const data = [
            {
                key:"wearing mask",
                amount: this.props.wearing,
                svg: { fill: '#0055FF' },
                // svg: { fill: '#0055FF' },
            },
            {
                key: "no mask",
                amount: this.props.not_wearing,
                svg: { fill: '#FFAA00' }
            }
            
        ]

        data.forEach(item => {
            // console.log(item)
            if (item.amount > 90) {

                if(item.key === "no mask"){
                    item.svg.fill = 'red';
                }
                
            }
        });

        const Labels = ({ slices, height, width }) => {
            return slices.map((slice, index) => {
                const { labelCentroid, pieCentroid, data } = slice;
                return (
                    <SvgText
                        key={index}
                        x={pieCentroid[ 0 ]}
                        y={pieCentroid[ 1 ]}
                        fill={'white'}
                        textAnchor={'middle'}
                        alignmentBaseline={'middle'}
                        fontSize={24}
                        stroke={'black'}
                        strokeWidth={0.2}
                        
                    >
                        {data.amount}
                    </SvgText>
                )
            })
        }

        return (
            <>
                <PieChart
                    style={{ height: 200 }}
                    valueAccessor={({ item }) => item.amount}
                    data={data}
                    spacing={0}
                    outerRadius={'95%'}
                >
                    <Labels/>
                </PieChart>

                <PieChartLegend data={data} />
            </>
        )
    }

}

const PieChartLegend = ({ data }) => {
    return (
                <View style={styles.legendContainer}>

            
            {data.map((item, index) => {
                // console.log(item)
                return (
                    <View key={index} style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }}>
                        <View style={{ backgroundColor: item.svg.fill, width: 20, height: 20, borderRadius: 10, marginRight: 5 }}></View>
                        <Text>{item.key}</Text>
                    </View>
                )
            })}
        </View>
    )
}

const MyPieChart = ( props: MaskProps ) => {
    console.log("from pie chart2:", props.text)
    const data = [
        {
            key: "wearing mask",
            amount: 20,
            svg: { fill: '#0055FF' },
        },
        {
            key: "No Mask",
            amount: 20,
            svg: { fill: '#FFAA00' }
        }
    ];

    return (
        <>
            <PieChartWithCenteredLabels data={data} />
            <PieChartLegend data={data} />
        </>
    )
}


const styles = StyleSheet.create({
    legendContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 20,
    },
    legendItem: {
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 30,
    },
    legendColor: {
      width: 20,
      height: 20,
      borderRadius: 10,
      marginRight: 5,
    },
    legendText: {
      fontSize: 16,
      fontWeight: 'bold',
      color: 'black',
    },
  })

// export default MyPieChart