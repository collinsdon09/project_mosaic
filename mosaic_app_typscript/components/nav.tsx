import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, TouchableOpacity, Button } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import MapsScreen from "./map";
import Circle from "./circle_safety";
import { useRoute } from "@react-navigation/native";
import axios from "axios";
import { PieChartWithCenteredLabels } from "./pie2";
import HistoryList from "./historyList";

// console.log(" Test2")
const safet_list = [];

const numbers_1 = [
  97, 4, 6, 1, 70, 3, 10, 8, 6, 2, 10, 10, 20, 12, 10, 10, 10, 10, 17, 0, 10, 6,
  10, 6, 10, 23, 40, 83, 10, 10, 49, 40, 39, 49, 1, 4, 5, 3, 2,
];

//const numbers = []

const generateSquares = (
  numbers,
  onPressSquare,
  navigation,
  facesFaceLabel,
  loc_name
) => {
  // const [history_safetyIdx, setHistorySafetyiidx = useState([]);
  // const [history_time, setHistoryTime] = useState(0);
  const [history_safetyIdx, setHistorySafetyidx] = useState("");
  const [history_time, setHistoryTime] = useState("");

  const [historyData, setHistoryData] = useState([]);

  console.log(" from generate square:", facesFaceLabel);

  // console.log(`Navigating to new Square with number ${number}`);

  const squares = numbers.map((number, index) => {
    // console.log("index:",index, "number:", number)
    let color = "red";
    if (number > 40 && number <= 59) {
      color = "orange";
    } else if (number > 60 && number <= 79) {
      color = "yellow";
    } else if (number > 80 && number <= 99) {
      color = "green";
    }
    return (
      <View>
        <TouchableOpacity
          key={index}
          onPress={() => {
            console.log(`Navigating to new Square with number ${number}`);

            // console.log("face2", facesFaceLabel)

            axios
              .get(
                `http://165.227.203.5/api-face-history/${loc_name}/face-label/${facesFaceLabel}`
              )

              .then((response) => {
                const historyArray = Object.values(response.data);
                const data = historyArray.flatMap((obj) =>
                  obj.flatMap((face_obj) =>
                    face_obj.scan.Faces.map((facex_obj) => ({
                      time: face_obj.scan.time,
                      safetyIdx: facex_obj.face_safety_indx,
                    }))
                  )
                );
                setHistoryData(data);

                console.log(data);
                navigation.navigate("HistoryPage", { data_name: data });
              })
              .catch((error) => {
                console.log(error);
              });
          }}
        >
          <View style={[styles.square, { backgroundColor: color }]}>
            <Text style={styles.text}>{number}</Text>
          </View>
        </TouchableOpacity>

        <Text>{history_safetyIdx}</Text>
      </View>
    );
  });
  return squares;
};
//SCREEN A
// const Squares = ({navigation}) => {

const Squares = ({ navigation }) => {
  const route = useRoute();
  const { loc_name } = route.params;

  const [facesSafetyIndex, setfacesSafetyIndex] = useState("");
  const [facesFaceLabel, setfacesFaceLabel] = useState("");

  const [numbers, setMyList] = useState([]);
  const [requestCount, setRequestCount] = useState(0);

  const [state, setState] = useState({
    policiesData: [],
    safetyIndex: [],
    wearingMasks: [],
    noMasks: [],
    cameraData: [],
  });

  useEffect(() => {
    // axios.get(`http://165.227.203.5/data/${loc_name}`)
    axios
      .get(`http://165.227.203.5/data-test/${loc_name}`)
      .then((response) => {
        setRequestCount(requestCount + 1);

        console.log(response.data);

        const dataArray: any = Object.values(response.data);

        dataArray.forEach((obj) => {
          //console.log(obj.city)
          // console.log(obj.Faces.face_safety_indx)
          // setfacesSafetyIndex()

          const f_safety_indx = obj.scan.Faces;

          obj.scan.Faces.forEach((face_obj) => {
            // console.log(face_obj.face_label);
            setfacesFaceLabel(face_obj.face_label);
            setfacesSafetyIndex(face_obj.face_safety_indx);
          });

          setState({
            ...state,
            policiesData: obj.policy,
            safetyIndex: obj.policy.safety_index,
            wearingMasks: obj.policy.wearing_mask,
            noMasks: obj.policy.no_mask,
            // facesSafetyindices:obj.Faces.face_safety_indx
          });
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  console.log("face index:", facesSafetyIndex);
  console.log("face label:", facesFaceLabel);

  if (!isNaN(parseInt(facesSafetyIndex))) {
    numbers.push(parseInt(facesSafetyIndex));
  }

  const onPressSquare = (number, facesFaceLabel) => {
    // Navigate to new page with the number as parameter
    //     axios.get(`http://165.227.203.5/api-face-history/${loc_name}/face-label/${facesFaceLabel}`)
    //   .then(response => {
    //   console.log(response.data);
    // })
    // .catch(error => {
    //   console.log(error);
    // });
  };

  return (
    <View>
      <Text style={styles.location_text}>{loc_name}</Text>

      <Circle safetyIndexProp={state.safetyIndex} />
      <View style={styles.container}>
        {generateSquares(numbers, onPressSquare, navigation, facesFaceLabel)}
      </View>

      <PieChartWithCenteredLabels
        wearing={parseInt(state.wearingMasks)}
        not_wearing={parseInt(state.noMasks)}
      />

      <Text>Number of requests made: {requestCount}</Text>
    </View>
  );
};

function DetailsScreen({ navigation, route }) {
  const { number } = route.params; // number from the squares world
  let circleColor = "red";
  if (number > 40 && number <= 59) {
    circleColor = "orange";
  } else if (number > 60 && number <= 79) {
    circleColor = "yellow";
  } else if (number > 80 && number <= 99) {
    circleColor = "green";
  }

  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <View style={[styles.circle, { backgroundColor: circleColor }]}>
        {/* <Text style={styles.number}>{number}</Text> */}
      </View>

      <Button title="Go to Home" onPress={() => navigation.navigate("Home")} />
      <Button title="Go back" onPress={() => navigation.goBack()} />
    </View>
  );
}

const handleCardPress = (title, navigation) => {
  console.log(`${title} card pressed!`);
  navigation.navigate("Details");
};

const Stack = createNativeStackNavigator();

function Main() {
  //main function
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={MapsScreen} />
        <Stack.Screen name="Details" component={Squares} />
        <Stack.Screen name="HistoryPage" component={HistoryList} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 0,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor:'black',
    height: 200,
    // marginBottom:200,
  },
  square: {
    width: 35,
    height: 35,
    margin: 3,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  text: {
    fontSize: 10,
    color: "white",
  },

  circle: {
    // width: 100,
    // height: 100,
    // borderRadius: 50,
    backgroundColor: "blue",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: 20,
    right: 20,
    width: 50,
    height: 50,
    borderRadius: 25,
  },

  container2: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f2f2f2",
  },

  location_text: {
    fontSize: 40,
    marginLeft: 30,
    marginTop: 30,
  },
});

export default Main;
