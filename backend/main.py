from fastapi import FastAPI
import pymongo


from models import Locations, policies_timeseries_data, Location_post, Camera
from mongoengine import connect
import json
import pydantic
from pydantic import BaseModel
from bson.objectid import ObjectId

pydantic.json.ENCODERS_BY_TYPE[ObjectId]=str


app = FastAPI()
# connect(db="mosaic", host="165.227.203.5", port=27017)
# connect(db="mosaic", host="localhost", port=27017)
client = pymongo.MongoClient("mongodb://localhost:27017/")

# Create a database object
mydb = client["mosaic_2"]


# Create a locations collection object
locations_collection = mydb["Locations"]

# Create an Policies collection object
policies_collection = mydb["Policies"]

# Create an Camera collection object
camera_collection = mydb["Camera"]














@app.get("/")
def home():
    return{"hellow done updating part2"}



# @app.get("/api-get-all-locations")
# def get_all_locations():
#     location_obj= Locations.objects().to_json()
#     locations= json.loads(location_obj)
#     return{"locations": locations}



# @app.get("/api-get-all-policies")
# def get_all_policies():
#     policies_obj= policies_timeseries_data.objects().to_json()
#     policies= json.loads(policies_obj)
#     return{"locations": policies}




@app.post("/post-data")
async def post_data(location: Location_post):
    incoming_location_data = dict(location)
    incoming_location_data['policy'] = dict(incoming_location_data['policy'])
    incoming_location_data['policy']['camera'] = dict(incoming_location_data['policy']['camera'])
    location_object_id=locations_collection.insert_one(incoming_location_data).inserted_id
    return {"message": f"location posted successfully, {location.policy}" }


#find by person name
@app.get("/data/{location_name}")
async def get_data(location_name: str):
    location_true = locations_collection.find_one({"location_name": location_name})
    if location_true:
        return location_true
    else:
        return {"message": f"Sorry, {location_true} was not found in the database."}




@app.get("/data")
async def get_all_data():
    all_locations = list(locations_collection.find())

   
    
    return {"data": all_locations   }


@app.delete("/delete-all")
async def delete_all_records():
    result = locations_collection.delete_many({})
    return {"message": f"{result.deleted_count} records deleted."}


