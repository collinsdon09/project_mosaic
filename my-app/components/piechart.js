
import React from 'react';
import Circle from '../components_2/circle_general_safety';
import {View, Text, ScrollView, StyleSheet, Dimensions} from 'react-native';
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
} from "react-native-chart-kit";


const data_2 = [
    {
        camera_id: "1",
        no_mask:"13",
        mask_range:"40.7166",
        wearing_mask: 108,
        color: "rgba(131, 167, 234, 1)",
        legendFontColor: "#7F7F7F",
        legendFontSize: 15
    }
    // },
    // {
    //     camera_id: "2",
    //     no_mask:"16",
    //     mask_range:"48.7166",
    //     wearing_mask: 108,
    //     color: "rgba(131, 167, 234, 1)",
    //     legendFontColor: "#7F7F7F",
    //     legendFontSize: 15
    // },
    // {
    //     camera_id: "3",
    //     no_mask:"18",
    //     mask_range:"88.7",
    //     wearing_mask: 108,
    //     color: "rgba(131, 167, 234, 1)",
    //     legendFontColor: "#7F7F7F",
    //     legendFontSize: 15
    // },
    // {
    //     camera_id: "4",
    //     no_mask:"10",
    //     mask_range:"50.7166",
    //     wearing_mask: 108,
    //     color: "rgba(131, 167, 234, 1)",
    //     legendFontColor: "#7F7F7F",
    //     legendFontSize: 15
    // },
    // {
    //     camera_id: "5",
    //     no_mask:"13",
    //     mask_range:"40.7166",
    //     wearing_mask: 108,
    //     color: "rgba(131, 167, 234, 1)",
    //     legendFontColor: "#7F7F7F",
    //     legendFontSize: 15
    // }
];


// const data = [
//     {
//         name: "Wearing masks",
//         population: 108,
//         color: "rgba(131, 167, 234, 1)",
//         legendFontColor: "#7F7F7F",
//         legendFontSize: 15
//     },
//     {
//         name: "Not_wearing_Wearing masks",
//         population: 33 ,
//         color: "#F00",
//         legendFontColor: "#7F7F7F",
//         legendFontSize: 15
//     },

// ];


const Pie_chart = (props) => {
    const { wearing_mask_prop } = props;
    console.log("wearing mask:", wearing_mask_prop)
    const data = [
        {
            name: "Wearing masks",
            population: wearing_mask_prop,
            // population: 20,

            color: "rgba(131, 167, 234, 1)",
            legendFontColor: "#7F7F7F",
            legendFontSize: 15
        },
        {
            name: "Not_wearing_Wearing masks",
            population: 20 ,
            color: "#F00",
            legendFontColor: "#7F7F7F",
            legendFontSize: 15
        },
    
    ];


    return(
        <View style={styles.pie_chart}>
            <Text> Location id: 1</Text>
            <PieChart
                data={data}
                width={Dimensions.get("window").width}
                height={220}
                // chartConfig={chartConfig}
                chartConfig={{
                    backgroundColor: "#e26a00",
                    backgroundGradientFrom: "#fb8c00",
                    backgroundGradientTo: "#ffa726",
                    decimalPlaces: 2, // optional, defaults to 2dp
                    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    style: {
                        borderRadius: 16
                    },
                    propsForDots: {
                        r: "6",
                        strokeWidth: "4",
                        stroke: "#ffa726"
                    }
                }}
                accessor={"population"}
                backgroundColor={"transparent"}
                paddingLeft={"45"}
                paddingBottom={"30"}
                center={[10, 10]}
                absolute
            />


        </View>


    )
}



const styles = StyleSheet.create({
    pie_chart:{
        marginLeft:0,
        marginTop:200,
        // backgroundColor:'black',

    }
})


export default Pie_chart;