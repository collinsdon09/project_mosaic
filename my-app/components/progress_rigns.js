
import React from 'react';
import {View, Text, ScrollView, StyleSheet, Dimensions} from 'react-native';
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
} from "react-native-chart-kit";

const data = {
    labels: ["wearing_mask", "No_mask"], // optional
    data: [108/141, 33/141]
};

const Progress_rings = () => {



    return(
        <View>
            <Text>mask_ratio:23.4042550| safety_index:86 | people_count:141 | </Text>
            <ProgressChart
                data={data}
                width={Dimensions.get("window").width}
                height={320}
                // chartConfig={chartConfig}
                chartConfig={{
                    backgroundColor: "#e26a00",
                    // backgroundGradientFrom: "#fb8c00",
                    // backgroundGradientTo: "#ffa726",
                    decimalPlaces:2 , // optional, defaults to 2dp
                    color: (opacity = 2) => `rgba(255, 255, 255, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    style: {
                        borderRadius: 16
                    },
                    propsForDots: {
                        r: "6",
                        strokeWidth: "4",
                        stroke: "#ffa726"
                    }
                }}
                accessor={"population"}
                backgroundColor={"transparent"}
                paddingLeft={"45"}
                paddingBottom={"30"}
                center={[10, 10]}
                absolute
            />


        </View>


    )
}


export default Progress_rings;