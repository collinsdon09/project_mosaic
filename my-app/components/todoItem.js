import React, { useState } from 'react';
import { StatusBar } from 'expo-status-bar';

import { Button, StyleSheet, Text, View, TextInput,ScrollView , TouchableOpacity, FlatList} from 'react-native';

export default function  TodoItem ({item, pressHandler}){
    return(
        <TouchableOpacity onPress={()=> pressHandler(item.key)}>
            <Text style={styles.item}>{item.text}</Text>
        </TouchableOpacity>
    )

}



const styles = StyleSheet.create({
    item: {
    padding:16,
    marginTop:16,
    borderColor:'green',
    borderWidth:1,
    borderStyle: 'dashed',
    borderRadius:10,
      
    },
});


