
import React from 'react';
import {View, Text, ScrollView, StyleSheet, Dimensions} from 'react-native';
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
} from "react-native-chart-kit";

const data = {
    labels: ["January", "February", "March", "April", "May", "June"],
    datasets: [
        {
            data: [20, 45, 28, 80, 99, 43]
        }
    ]
};


const Bar_chart = () => {



    return(
        <View>

            <BarChart
                // style={graphStyle}
                      style={{
                        marginVertical: 8,
                        borderRadius: 16
                      }}
                        data={data}
                      width={Dimensions.get("window").width}
                height={220}
                yAxisLabel="$"

                      chartConfig={{
                         backgroundColor: "#e26a00",
                        backgroundGradientFrom: "#fb8c00",
                         backgroundGradientTo: "#ffa726",
                      decimalPlaces: 2, // optional, defaults to 2dp
                  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                 style: {
                       borderRadius: 16
                   },
                    propsForDots: {
                       r: "6",
                       strokeWidth: "2",
                        stroke: "#ffa726"
                    }
                      }}
            />

        </View>


    )
}


export default Bar_chart;