




// import React from 'react';
// import { View, Text, StyleSheet } from 'react-native';
//
// const generateSquares = (numbers) => {
//     const squares = numbers.map((number, index) => {
//         let color = 'red';
//         if (number > 40 && number <= 59) {
//             color = 'orange';
//         }else if (number > 60 && number <= 79) {
//             color = 'yellow';
//         }else if (number > 80 && number <= 99) {
//         color = 'green';
//     }
//         return (
//             <View key={index} style={[styles.square, { backgroundColor: color }]}>
//                 <Text style={styles.text}>{number}</Text>
//             </View>
//         );
//     });
//     return squares;
// };
//
// const Squares_1= () => {
//     const numbers = [97, 4, 6, 1, 70, 3, 10, 8, 6, 2, 10, 10, 20, 12, 10, 10, 10, 10,
//         17, 0, 10, 6, 10, 6, 10, 23, 40, 83, 10, 10, 49, 40,
//         39, 49, 1, 4, 5, 3, 2];
//     return (
//         <View style={styles.container}>
//             {generateSquares(numbers)}
//         </View>
//     );
// };
//
// const styles = StyleSheet.create({
//     container: {
//         marginTop:100,
//         flex: 0,
//         flexDirection: 'row',
//         flexWrap: 'wrap',
//         justifyContent: 'center',
//         alignItems: 'center',
//     },
//     square: {
//         width: 35,
//         height: 35,
//         margin: 3,
//         alignItems: 'center',
//         justifyContent: 'center',
//         borderRadius: 10, // Add border radius of 10
//     },
//     text: {
//         fontSize: 10,
//         color: 'white',
//     },
// });
//
// export default Squares_1;

import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const generateSquares = (numbers, onPressSquare) => {
    const squares = numbers.map((number, index) => {
        let color = 'red';
        if (number > 40 && number <= 59) {
            color = 'orange';
        } else if (number > 60 && number <= 79) {
            color = 'yellow';
        } else if (number > 80 && number <= 99) {
            color = 'green';
        }
        return (
            <TouchableOpacity key={index} onPress={() => onPressSquare(number)}>
                <View style={[styles.square, { backgroundColor: color }]}>
                    <Text style={styles.text}>{number}</Text>
                </View>
            </TouchableOpacity>
        );
    });
    return squares;
};

const Squares = () => {
    const numbers = [97, 4, 6, 1, 70, 3, 10, 8, 6, 2, 10, 10, 20, 12, 10, 10, 10, 10,
        17, 0, 10, 6, 10, 6, 10, 23, 40, 83, 10, 10, 49, 40,
        39, 49, 1, 4, 5, 3, 2];

    const onPressSquare = (number) => {
        // Navigate to new page with the number as parameter
        console.log(`Navigating to new page with number ${number}`);
    }

    return (
        <View style={styles.container}>
            {generateSquares(numbers, onPressSquare)}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginTop: 100,
        flex: 0,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center',
    },
    square: {
        width: 35,
        height: 35,
        margin: 3,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
    },
    text: {
        fontSize: 10,
        color: 'white',
    },
});

export default Squares;
