
import React from 'react';
import {View, Text, ScrollView, StyleSheet, Dimensions} from 'react-native';
// import MathText from 'react-native-math';

import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
} from "react-native-chart-kit";

const data = {
    labels: ["location_id:1", "Location_id:22"],
    legend: ["wearing_masks", "No_mask", "L3"],
    data: [
        [((108/141)*100).toFixed(2), ((33/141)*100).toFixed(2)], //location_id: 1
        [50, (192/254)*100]  //location_id: 2
        // [(62/254)*100, (192/254)*100]  //location_id: 2
    ],
    barColors: ["#FFBF00", "#FBCEB1", "#CD7F32"]
};

const Stack_bar = () => {



    return(
        <View>
            <Text>mask_ratio:23.4042550| safety_index:86 | people_count:141 | </Text>
            <StackedBarChart
                data={data}
                width={Dimensions.get("window").width}
                height={320}
                // chartConfig={chartConfig}
                chartConfig={{
                    backgroundColor: "#e26a00",
                    // backgroundGradientFrom: "#fb8c00",
                    // backgroundGradientTo: "#ffa726",
                    decimalPlaces:0 , // optional, defaults to 2dp
                    color: (opacity = 2) => `rgba(255, 255, 255, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    style: {
                        borderRadius: 16
                    },
                    propsForDots: {
                        r: "6",
                        strokeWidth: "4",
                        stroke: "#ffa726"
                    }
                }}
                accessor={"population"}
                backgroundColor={"transparent"}
                paddingLeft={"45"}
                paddingBottom={"30"}
                center={[10, 10]}
                absolute
            />


        </View>


    )
}


export default Stack_bar;