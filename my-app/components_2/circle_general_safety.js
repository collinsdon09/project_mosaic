// import React, { Component } from 'react';
// import { View, StyleSheet , Text} from 'react-native';

// export default class Circle extends Component {
//   render() {
//     return (
//       <View style={styles.circle} >

// <Text style={styles.text}>50</Text>
//         </View>
      
      
//     );
//   }
// }

// const styles = StyleSheet.create({
//   circle: {
//     width: 100,
//     height: 100,
//     borderRadius: 50,
//     backgroundColor: '#f00',
//     justifyContent: 'center',
//     alignItems: 'center',
//   },

//   text: {
//     color: '#fff',
//     fontSize: 32,
//     fontWeight: 'bold',
//   },
// });



import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';

export default class Circle extends Component {
  render() {
    const { safety_index_prop } = this.props;
    // let circleColor = '#f00';
    // if (number === 50) {
    //   circleColor = '#0f0';
    // }
    let circleColor = 'red';
        if (safety_index_prop > 40 && safety_index_prop <= 59) {
            circleColor = 'orange';
        } else if (safety_index_prop > 60 && safety_index_prop <= 79) {
            circleColor = 'yellow';
        } else if (safety_index_prop > 80 && safety_index_prop <= 100) {
            circleColor = 'green';
        }
    return (

      <View style={[styles.circle, { backgroundColor: circleColor }]}>
        <Text> Safety index</Text>
        <Text style={styles.text}>{safety_index_prop}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  circle: {
    marginTop:10,
    marginBottom:50,
    marginLeft:300,
    width: 100,
    height: 100,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 32,
    fontWeight: 'bold',
  },
});
