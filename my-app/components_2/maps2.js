import React from 'react';
import MapView, {PROVIDER_GOOGLE,Marker} from 'react-native-maps';
import { Dimensions, StyleSheet, View } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
// import DetailsScreen from './DetailsScreen';
import Pie_chart from '../components/piechart';


let locationsOfInterest = [
  {
    title:"First",
    location:{
      latitude:39.042031,
      longitude:-94.588658
    },
    description: "location_ID:1"
  },
  {
    title:"second",
    location:{
      latitude:-1.2321906,
      longitude:36.8778405
    },
    description: "location_ID:2"
  }
];

const {width, height} = Dimensions.get("window")
const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 50.0;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO
const INITIAL_POSITION = {
  latitude: 37.6,
  longitude:-95.665,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta:LONGITUDE_DELTA,
}


export default function Maps_screen2({ navigation }) {

  const onRegionChange = (region) => {
    console.log(region)
  }

  const showLocationsOfInterest = (number) => {
    return locationsOfInterest.map((item, index) => {
      return(
        <Marker
          key={index}
          coordinate={item.location}
          title={item.title}
          description={item.description}
          onPress={() => navigation.navigate('Details', {number})}
        />
      )
    })
  }

  return (
    <View style={styles.container}>
      <MapView 
        style={styles.map}
        onRegionChange={onRegionChange}
        provider={PROVIDER_GOOGLE}
        initialRegion={INITIAL_POSITION} 
      >
        {showLocationsOfInterest()}
      </MapView>

      <View style={styles.searchContainers}>
        {/* <GooglePlacesAutocomplete
          styles={{textInput:styles.input}}
          placeholder='Search'
          onPress={(data, details = null) => {
            // if (data?.geometry?.location) {
              console.log(data.region)
              // const { lat, lng } = data.geometry.location;
              // console.log(data.geometry.location)
              // const newRegion = {
              //   latitude: lat,
              //   longitude: lng,
              //   latitudeDelta: LATITUDE_DELTA,
              //   longitudeDelta: LONGITUDE_DELTA,
              // };

            // console.log(details);
          }}
          query={{
            key: 'AIzaSyCiYS_YapjH0QVzRN8UYH3MvTwBR6ip_co',
            language: 'en',
          }}
        /> */}
      </View>

      {/* <Pie_chart /> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    width: '100%',
    height: '100%',
  },
  searchContainers:{
    position:"absolute",
    width:"90%",
    backgroundColor:"white",
    shadowColor:"black",
    shadowOffset:{width:2, height: 2},
    shadowOpacity:0.5,
    shadowRadius:4,
    elevation:4,
    padding:8,
    borderRadius:8,
  },
  input:{
    borderColor:"#888",
    borderWidth:1,
  }
});

