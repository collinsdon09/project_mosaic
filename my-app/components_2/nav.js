
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity,Button } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Pie_chart from '../components/piechart';
import MapsScreen from './maps3';
import Locale from './locations';
import Circle from './circle_general_safety';
import { useRoute } from '@react-navigation/native';
import axios from 'axios';


// import Card from './card';


const generateSquares = (numbers, onPressSquare, navigation) => {
    const squares = numbers.map((number, index) => {
        let color = 'red';
        if (number > 40 && number <= 59) {
            color = 'orange';
        } else if (number > 60 && number <= 79) {
            color = 'yellow';
        } else if (number > 80 && number <= 99) {
            color = 'green';
        }
        return (
            <TouchableOpacity key={index} onPress={() => onPressSquare(number, navigation)}>
                <View style={[styles.square, { backgroundColor: color }]}>
                    <Text style={styles.text}>{number}</Text>
                </View>

             </TouchableOpacity>


        );
    });
    return squares;
};

const Squares = ({navigation}) => {


  let  policies_data= {
    safety_index : "",
    Wearing_mask : "",
    no_mask : "",
    time: "",
  }


  const route = useRoute();
  const { loc_name } = route.params;
  console.log("location_name:", loc_name)

  const [policiesData, setPoliciesData] = useState([]);
  const [safetyIndex, setSafetyIndex] = useState([]);
  const [wearingMasks, setWearingMask] = useState([]);
  const [noMasks, setNoMask] = useState([]);



  const [cameraData, setCameraData] = useState([]);

  // const [safet_index, setSafetyIndex] = useState([]);


  // useEffect (()=>{
  //   const apiEndpoint = `http://165.227.203.5/data/${loc_name}`

  //   axios.get(apiEndpoint)
  //   .then(response => {

  //     // console.log(response.data.policy)


  //     let  policies_data= {
  //       safety_index : response.data.policy.safety_index,
  //       Wearing_mask : response.data.policy.wearing_mask,
  //       no_mask : response.data.policy.no_mask,
  //       time: response.data.policy.time,
  //     }


  //     let  camera_data= {
  //       camera_model_number : response.data.policy.camera.camera_model_number,
  //       camera_manufacturer : response.data.policy.camera.camera_manufacturer,
       
  //     }

  //     console.log(camera_data)
  //     setResponse(response.data); // update the response state




  //   })
  // })


  useEffect(() => {
    axios.get(`http://165.227.203.5/data/${loc_name}`)
      .then(response => {
        // console.log(response.data)
        setPoliciesData(response.data.policy);
        setSafetyIndex(response.data.policy.safety_index)
        setWearingMask(response.data.policy.wearing_mask)
        setNoMask(response.data.policy.no_mask)

      })
      .catch(error => {
        console.log(error);
      });
  }, []);


 




    const numbers = [97, 4, 6, 1, 70, 3, 10, 8, 6, 2, 10, 10, 20, 12, 10, 10, 10, 10,
        17, 0, 10, 6, 10, 6, 10, 23, 40, 83, 10, 10, 49, 40,
        39, 49, 1, 4, 5, 3, 2];

    const onPressSquare = (number) => {
        // Navigate to new page with the number as parameter
      console.log(`Navigating to new Square with number ${number}`); 
    }





    return ( 

      
        <View>

      


        <Circle style={{ marginTop: 0 }} safety_index_prop={safetyIndex} />
        

        {/* <Pie_chart wearing_mask_prop={wearingMasks}/> */}
        <View style={styles.container}>
        
            {generateSquares(numbers, onPressSquare, navigation)}

        
            <Pie_chart wearing_mask_prop={wearingMasks} noMask_prop={noMasks}/>

        </View>

        </View>
    );
};



function DetailsScreen({ navigation, route }) {
    const {number} = route.params // number from the squares world
    let circleColor = 'red';
  if (number > 40 && number <= 59) {
    circleColor = 'orange';
  } else if (number > 60 && number <= 79) {
    circleColor = 'yellow';
  } else if (number > 80 && number <= 99) {
    circleColor = 'green';
  }


  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center"}}>
    
      <View style={[styles.circle, { backgroundColor: circleColor }]}>
        <Text style={styles.number}>{number}</Text>
      </View>



      <Button title="Go to Home" onPress={() => navigation.navigate("Home")} />
      <Button title="Go back" onPress={() => navigation.goBack()} />
    </View>
  );
}


    const handleCardPress = (title, navigation) => {
      console.log(`${title} card pressed!`);
      navigation.navigate("Details")

    };



const Stack = createNativeStackNavigator();

function Main(){ //main function
    return(
        <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={MapsScreen} />
        <Stack.Screen name="Details" component={Squares} />
      </Stack.Navigator>
    </NavigationContainer>



    )
}







const styles = StyleSheet.create({
    container: {
        flex: 0,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor:'black',
        height:200,
        // marginBottom:200,
    },
    square: {
        width: 35,
        height: 35,
        margin: 3,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
    },
    text: {
        fontSize: 10,
        color: 'white',
    },

    circle: {
        // width: 100,
        // height: 100,
        // borderRadius: 50,
        backgroundColor: 'blue',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
            top: 20,
            right: 20,
            width: 50,
            height: 50,
            borderRadius: 25,
            alignItems: 'center',
            justifyContent: 'center',
      },

      container2: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f2f2f2',
      },

    
});

export default Main;

