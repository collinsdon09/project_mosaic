import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Card from './card';

const App = () => {
  const handleCardPress = (title) => {
    console.log(`${title} card pressed!`);
  };

  return (
    <View style={styles.container}>
      <Card
        title="Card 1"
        description="Click me!"
        onPress={() => handleCardPress("Card 1")}
      />
      <Card
        title="Card 2"
        description="Click me!"
        onPress={() => handleCardPress("Card 2")}
      />
      <Card
        title="Card 3"
        description="Click me!"
        onPress={() => handleCardPress("Card 3")}
      />
      <Card
        title="Card 4"
        description="Click me!"
        onPress={() => handleCardPress("Card 4")}
      />
      <Card
        title="Card 5"
        description="Click me!"
        onPress={() => handleCardPress("Card 5")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f2f2f2',
  },
});

export default App;
