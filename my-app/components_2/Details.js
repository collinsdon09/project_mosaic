import React from 'react';
import { View, Text } from 'react-native';

export default function DetailsScreen({ route }) {
  const { _id } = route.params;

  return (
    <View>
      <Text>Details Screen</Text>
      <Text>_id: {JSON.stringify(_id)}</Text>
    </View>
  );
}