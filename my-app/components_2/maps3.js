import React, { useState, useEffect } from 'react';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { Dimensions, StyleSheet, View } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import axios from 'axios';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 50.0;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const INITIAL_POSITION = {
  latitude: 37.6,
  longitude: -95.665,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA,
};

export default function MapsScreen({navigation}) {
  const [markers, setMarkers] = useState([]);

  useEffect(() => {
    // Define the API endpoint from where you want to fetch coordinates
    const apiEndpoint = 'http://165.227.203.5/data';
    // const apiEndpoint = 'http://localhost/data';
    
    axios.get(apiEndpoint)
      .then(response => {
        // console.log(response.data)
        const dataArray = Object.values(response.data);
        let locationsOfInterest = [];

        dataArray[0].forEach(obj => {
          let newLocation = {
            _id : obj._id,
            title: obj.location_name,
            location: {
              latitude: parseFloat(obj.gps_latitude),
              longitude: parseFloat(obj.gps_longitude)
            },
            description: obj.city
          };
          
          locationsOfInterest.push(newLocation);
        });
        
        setMarkers(locationsOfInterest);
      })
      .catch(error => {
        console.error('Error fetching coordinates:', error);
      });
  }, []);

  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        provider={PROVIDER_GOOGLE}
        initialRegion={INITIAL_POSITION}
        >
        {markers.map((location, index, _id) => (
          <Marker
            key={index}
            coordinate={location.location}
            title={location.title}
            description={location.description}
            // onPress={() => navigation.navigate('Details', {_id: location._id})}
            onPress={() => navigation.navigate('Details', {loc_name: location.title})}

         
          />
        ))}
      </MapView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    width: '100%',
    height: '100%',
  },
});
